"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import math
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# brackets must be ordered, farther -> nearer cp pace brackets
pace_brackets = [[1000, 13.333, 26], [600, 11.428, 28], [400, 15, 30], [200, 15, 32], [0, 15, 34]]
max_time_for_brevets = { 200: 810, 300: 1200, 400: 1620, 600: 2400, 1000: 4500}
START = 0
MIN_SPEED_KM = 1
MAX_SPEED_KM = 2

def elapsed_time_at_speed(control_dist, brevet_dist, speed):
	race_time = 0
	dist_left = control_dist

	for bracket in pace_brackets:
		if bracket[START] > dist_left:
			continue

		leg = dist_left - bracket[START]
		race_time += leg / bracket[speed]
		dist_left -= leg

	time_minutes = round(race_time * 60)

	return time_minutes

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

	"""
	Args:
	   control_dist_km:  number, the control distance in kilometers
	   brevet_dist_km: number, the nominal distance of the brevet
		   in kilometers, which must be one of 200, 300, 400, 600,
		   or 1000 (the only official ACP brevet distances)
	   brevet_start_time:  An ISO 8601 format date-time string indicating
		   the official start time of the brevet
	Returns:
	   An ISO 8601 format date string indicating the control open time.
	   This will be in the same time zone as the brevet start time.
	"""

	minutes_after_start = elapsed_time_at_speed(control_dist_km, brevet_dist_km, MAX_SPEED_KM)
	return arrow.get(brevet_start_time).shift(minutes=+minutes_after_start).isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
	"""
	Args:
	   control_dist_km:  number, the control distance in kilometers
		  brevet_dist_km: number, the nominal distance of the brevet
		  in kilometers, which must be one of 200, 300, 400, 600, or 1000
		  (the only official ACP brevet distances)
	   brevet_start_time:  An ISO 8601 format date-time string indicating
		   the official start time of the brevet
	Returns:
	   An ISO 8601 format date string indicating the control close time.
	   This will be in the same time zone as the brevet start time.
	"""
	# the start control closes one hour in
	if control_dist_km == 0:
		return arrow.get(brevet_start_time).shift(hours=+1).isoformat()

	# controls beyond the official distance close at max time for that brevet
	if control_dist_km > brevet_dist_km:
		minutes_after_start = max_time_for_brevets[brevet_dist_km]
	else:
		minutes_after_start = elapsed_time_at_speed(control_dist_km, brevet_dist_km, MIN_SPEED_KM)
		
	return arrow.get(brevet_start_time).shift(minutes=+minutes_after_start).isoformat()
