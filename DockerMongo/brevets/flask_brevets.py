"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

from pymongo import MongoClient
from bson.json_util import dumps

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controls

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
	app.logger.debug("Main page entry")
	return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
	app.logger.debug("Page not found")
	flask.session['linkback'] = flask.url_for("index")
	return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
	"""
	Calculates open/close times from kilometres, using rules
	described at https://rusa.org/octime_alg.html.
	Expects one URL-encoded argument, the number of kilometres.
	"""
	app.logger.debug("Got a JSON request")
	km = request.args.get('km', 999, type=float)
	brevet_dist = request.args.get('brevet_dist_km', type=int)
	datetime_string = request.args.get('start_time', type=str)

	app.logger.debug("km={}".format(km))
	app.logger.debug("request.args: {}".format(request.args))
	# FIXME: These probably aren't the right open and close times
	# and brevets may be longer than 200km
	open_time = acp_times.open_time(km, brevet_dist, arrow.get(datetime_string).isoformat())
	close_time = acp_times.close_time(km, brevet_dist, arrow.get(datetime_string).isoformat())
	result = {"open": open_time, "close": close_time}
	return flask.jsonify(result=result)

@app.route("/_submit_row", methods=['POST'])
def _submit_row():
	row = request.args.get("row")
	km = request.args.get("km")
	db.rows.update_one({"id": row}, {"$set": {"km": km}}, True )
	result = {"status": "OK"}
	return flask.jsonify(result=result)

@app.route("/display")
def display():
	return flask.render_template('display.html'), 200

@app.route("/_retrieve_row")
def _retrieve_row():
	row_id = request.args.get("row")
	row_obj = db.rows.find({"id": row_id})
	return flask.jsonify(dumps(row_obj))
#############

app.debug = CONFIG.DEBUG
if app.debug:
	app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
	print("Opening for global access on port {}".format(CONFIG.PORT))
	app.run(port=CONFIG.PORT, host="0.0.0.0")
